output "vpc_id" {
  value = aws_vpc.sys_vpc.id
}

output "cidr_block" {
  value = aws_vpc.sys_vpc.cidr_block
}

output "public_subnet_cidr" {
  value = aws_subnet.sys_public_subnet[*].cidr_block
}

output "private_subnet_cidr" {
  value = aws_subnet.sys_private_subnet[*].cidr_block
}