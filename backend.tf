terraform {
  cloud {
    organization = "systtekcloud"

    workspaces {
      name = "sys-terransible"
    }
  }
}